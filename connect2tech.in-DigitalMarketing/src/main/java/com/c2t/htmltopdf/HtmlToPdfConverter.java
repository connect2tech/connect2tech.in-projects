package com.c2t.htmltopdf;

import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;

public class HtmlToPdfConverter {

	public static void main(String[] args) {

		try {

			/**
			 * Creating an instance of iText renderer which will be used to
			 * generate the pdf from the html document.
			 */
			final ITextRenderer iTextRenderer = new ITextRenderer();

			/**
			 * Setting the document as the url value passed. This means that the
			 * iText renderer has to parse this html document to generate the
			 * pdf.
			 */
			iTextRenderer.setDocument("file:///D:/nchaurasia/Java-Architect/connect2tech.in-digitalmarketing/src/main/resources/certificate/Java-Selenium-Cert.html");
			iTextRenderer.layout();

			/**
			 * The generated pdf will be written to the invoice.pdf file.
			 */
			final FileOutputStream fileOutputStream = new FileOutputStream(new File("Cert.pdf"));

			/**
			 * Creating the pdf and writing it in invoice.pdf file.
			 */
			iTextRenderer.createPDF(fileOutputStream);
			fileOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
