# Java J2ee

### D:\nc\Java-Architect ###
 * connect2tech.in-drools-engine-framework
 * connect2tech.in-drools-michalbali
 * connect2tech.in-java-j2ee-architect
 * connect2tech.in-jpetstore
---
### D:\nc\eclipse-workspace ###
 * connect2tech
 * Java-Architect
---
### D:\nc\Java-Architect\connect2tech.in-java-j2ee-architect ###
 * connect2tech.in-Java-ExtentReports
 * connect2tech.in-Java-J2ee-Algorithms
 * connect2tech.in-Java-J2ee-CoreJava
 * connect2tech.in-Java-J2ee-DesignPatterns
 * connect2tech.in-Java-J2ee-Drools-Training
 * connect2tech.in-Java-J2ee-Hamcrest
 * connect2tech.in-Java-J2ee-JSON
 * connect2tech.in-Java-J2ee-Junit-TestNG-MockIt
 * connect2tech.in-Java-J2ee-Mockito
 * connect2tech.in-Java-J2ee-OOAD
 * connect2tech.in-Java-J2ee-SOAP-RPC-Document
 * connect2tech.in-Java-J2ee-TestDataManagement
 * connect2tech.in-Java-J2ee-web
 * connect2tech.in-Java-J2ee-XML
 * connect2tech.in-Java-Selenium-3.x
 * connect2tech.in-Java-Serenity-Framework
---

 - in.connect2tech.website.topic
   - in.connect2tech.vogella.hamcrest

- in.connect2tech.kathy.java5.generics

---
 
| Topic       | Pending     | Done        |Comments    |
| ----------- | ----------- | ----------- |----------- |
| JSON        | Title       | Title       |Title       |
| Mocking     | Text        | Text        |Text        |
| XML   | Text        | Text        |Text        |
